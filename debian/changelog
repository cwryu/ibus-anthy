ibus-anthy (1.5.11-1) unstable; urgency=medium

  * Team upload.
  * debian/control:
    + Bump Standards-Version to 4.4.0.
    + Bump debhelper compat to v12.
  * debian/patches:
    - Drop patch 0002-0008, merged upstream.
    + Refresh patch 0001 around using utf-8.
  * debian/symbols: Add one new symbol.

 -- Boyuan Yang <byang@debian.org>  Fri, 20 Sep 2019 15:02:12 -0400

ibus-anthy (1.5.10-2) unstable; urgency=medium

  * Team upload.
  * Rebuild against python3.7 as default python3 interpreter.
  * debian/patches:
    + Backport upstream patches till 2018-09-13.
    + Add patch to install appdata files in the new metainfo
      directory.
  * debian/control: Update Homepage field.

 -- Boyuan Yang <byang@debian.org>  Wed, 05 Dec 2018 10:40:17 -0500

ibus-anthy (1.5.10-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 1.5.10.
  * debian: Apply "wrap-and-sort -abst".
  * debian/control:
    + Remove unnecessary build-dep dh-autoreconf.
    + Bump Standards-Version to 4.2.1.
    + Update Vcs fields and use the repo under input-method-team.
  * debian/rules:
    + Use "dh_missing --fail-missing".
    + Disable test by default since it needs running ibus instance.
  * debian/copyright: Refresh information.
  * debian/patches: Drop patch for appstream fixing, merged upstream.

 -- Boyuan Yang <byang@debian.org>  Tue, 04 Sep 2018 13:33:17 -0400

ibus-anthy (1.5.9-3) unstable; urgency=medium

  * Update VCS to new salsa service in compliance to the Debian
    Policy 5.6.26 (version 4.1.4).
  * Migrate from alioth to lists.debian.org for the maintainer address.
    Closes: #899538
  * Bump standards version to 4.1.4 and compat to 11.
  * Format with wrap-and-sort.

 -- Osamu Aoki <osamu@debian.org>  Sun, 08 Jul 2018 07:59:33 +0900

ibus-anthy (1.5.9-2.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Open anthy zip dictionary file as utf-8, closed #873860

 -- NOKUBI Takatsugu <knok@daionet.gr.jp>  Thu, 02 Nov 2017 15:16:50 +0900

ibus-anthy (1.5.9-2) unstable; urgency=medium

  * Update uploaders list to current ones.  Closes: #841800
  * Install appstream metadata with its upstream fix to the
    metadata.  Closes: #858076

 -- Osamu Aoki <osamu@debian.org>  Thu, 06 Apr 2017 23:31:44 +0900

ibus-anthy (1.5.9-1) unstable; urgency=medium

  * New upstream release.

 -- Osamu Aoki <osamu@debian.org>  Wed, 07 Dec 2016 01:23:09 +0900

ibus-anthy (1.5.6-1) unstable; urgency=medium

  * New upstream release.

 -- Osamu Aoki <osamu@debian.org>  Thu, 16 Oct 2014 00:52:45 +0900

ibus-anthy (1.5.5-2) unstable; urgency=medium

  * Use --with-python=python3 to avoid the compatibility problem
    and migrate to the Python 3.  Closes: #760478 (LP: #1355141)

 -- Osamu Aoki <osamu@debian.org>  Sun, 07 Sep 2014 20:03:05 +0900

ibus-anthy (1.5.5-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Clean up install script.
  * Differ multiarch to avoid package splitting.

 -- Osamu Aoki <osamu@debian.org>  Sun, 27 Jul 2014 23:40:28 +0900

ibus-anthy (1.5.4-2) unstable; urgency=low

  * Team upload

  [ Matthias Klose ]
  * Build using dh-autoreconf

  [ Ikuya Awashiro ]
  * Set 'default' layout instead jp layout. (LP: #1279845)

  [ Aron Xu ]
  * std-ver: 3.9.4 -> 3.9.5, no change required

 -- Aron Xu <aron@debian.org>  Mon, 10 Mar 2014 16:30:06 +0800

ibus-anthy (1.5.4-1) unstable; urgency=low

  * Team upload.
  * New upstream release.
  * Handle .la with dh_install -X.

 -- Osamu Aoki <osamu@debian.org>  Sun, 20 Oct 2013 09:23:10 +0900

ibus-anthy (1.5.3-3) unstable; urgency=low

  * ibus 1.5 transition build.
  * Fix git repo URL.

 -- Osamu Aoki <osamu@debian.org>  Sat, 07 Sep 2013 23:46:17 +0900

ibus-anthy (1.5.3-2) experimental; urgency=low

  * Updated copyright holders.
  * Make ibus-anthy non-multiarch for now since
    gobject-introspection does not support mulit-arch.
    Thanks Mitsuya.  Closes: #720071

 -- Osamu Aoki <osamu@debian.org>  Tue, 20 Aug 2013 21:13:48 +0900

ibus-anthy (1.5.3-1) experimental; urgency=low

  [ Osamu Aoki ]
  * Team upload.
  * New upstream release
  * compat=9 and multi-arch updates etc.
  * Add symbols and lintian-overrides file.
  * Updated package description for -dev package.

  [ Aron Xu ]
  * debian/control:
    - DMUA: removed.
    - Std-ver: 3.9.3 -> 3.9.4, no change.
    - B-D: updated.

 -- Osamu Aoki <osamu@debian.org>  Sun, 14 Jul 2013 01:01:12 +0900

ibus-anthy (1.4.99.20120327-1) experimental; urgency=low

  * New upstream release

 -- Asias He <asias.hejun@gmail.com>  Fri, 05 Oct 2012 14:23:38 +0800

ibus-anthy (1.2.7-1) unstable; urgency=low

  * New upstream release

 -- Asias He <asias.hejun@gmail.com>  Thu, 16 Aug 2012 17:28:16 +0800

ibus-anthy (1.2.6-2) unstable; urgency=low

  * Used "dh $@ --with python2 --with autotools-dev"
  * Removed *.la files.
  * Added DEP-3 patch tagging and DEP-5 copyright file.

 -- Osamu Aoki <osamu@debian.org>  Sun, 24 Jul 2011 19:40:28 +0900

ibus-anthy (1.2.6-1) unstable; urgency=low

  * New upstream release
  * Bump standards version to 3.9.2
  * Use dh style debian/rules

 -- Asias He <asias.hejun@gmail.com>  Sun, 29 May 2011 12:45:56 +0800

ibus-anthy (1.2.5-2) unstable; urgency=low

  * Fix "Use of PYTHONPATH env var in an insecure way"
   (Closes: #605171) (Closes: #605172)
  * Set Vcs to git.debian.org

 -- Asias He <asias.hejun@gmail.com>  Tue, 22 Mar 2011 21:47:54 +0800

ibus-anthy (1.2.5-1) unstable; urgency=low

  [ Asias He ]
  * New upstream release.

  [ LI Daobing ]
  * debian/rules: update clean.

 -- Asias He <asias.hejun@gmail.com>  Fri, 03 Dec 2010 22:27:30 +0800

ibus-anthy (1.2.3-1) unstable; urgency=low

  [ Asias He ]
  * New upstream release.
  * debian/control
    - bump standards version to 3.9.1.
    - add Asias He to uploaders.

 -- LI Daobing <lidaobing@debian.org>  Mon, 18 Oct 2010 20:29:28 +0800

ibus-anthy (1.2.1-1) unstable; urgency=low

  * New upstream release.
    - debian/patches/broken-link: removed.
    - debian/patches/zipcode-t-dir: removed.
  * debian/control:
    - set maintainer to IME team, add me to uploaders.
    - update Vcs-* fields.

 -- LI Daobing <lidaobing@debian.org>  Thu, 29 Apr 2010 20:21:17 +0800

ibus-anthy (1.2.0.20100313-2) unstable; urgency=low

  * debian/source/format: 3.0.
  * Fix "ibus-anthy fails to start with "File exist" error" (Closes: #575540)
    - debian/patches/broken-link: also remove broken link in ~/.anthy
    - debian/patches/zipcode-t-dir: change the dir of zipcode.t

 -- LI Daobing <lidaobing@debian.org>  Sat, 10 Apr 2010 18:39:23 +0800

ibus-anthy (1.2.0.20100313-1) unstable; urgency=low

  * New upstream release.
  * debian/control: bump standards version to 3.8.4.

 -- LI Daobing <lidaobing@debian.org>  Sun, 21 Mar 2010 18:36:23 +0800

ibus-anthy (1.2.0.20100115-1) unstable; urgency=low

  * New upstream release.
    - fix convert_to_half do same as convert_to_half_katakana (LP: #475031)

 -- LI Daobing <lidaobing@debian.org>  Sun, 24 Jan 2010 16:17:50 +0800

ibus-anthy (1.2.0.20091127-1) unstable; urgency=low

  * New upstream release.

 -- LI Daobing <lidaobing@debian.org>  Sat, 28 Nov 2009 14:15:30 +0800

ibus-anthy (1.2.0.20090917-1) unstable; urgency=low

  * new upstream release.

 -- LI Daobing <lidaobing@debian.org>  Thu, 17 Sep 2009 19:37:06 +0800

ibus-anthy (1.2.0.20090907-1) unstable; urgency=low

  * new upstream release.
  * debian/rules: update clean rule.

 -- LI Daobing <lidaobing@debian.org>  Mon, 14 Sep 2009 21:23:56 +0800

ibus-anthy (1.2.0.20090813-2) unstable; urgency=low

  [ Loïc Minier ]
  * Drop useless CROSS logic in rules; build uses dh_auto_configure.
  * Drop /usr/share/misc/config.{sub,.guess} conditionals since these are
    always present (thanks to autotools-dev bdep).
  * Drop bogus ibus-anthy.lintian-overrides since the binary truly lacks a
    manpage.
  * Pass -s to dh_* in binary-arch.
  * Drop non-existent clean0 from .PHONY.
  * Cleanup rules droppping boilerplate comments and superfluous whitespace.
  * Add XS-Python-Version/XB-Python-Version: fields.
  * Version the python-dev bdep to >= 2.5.
  * Drop useless autoconf, automake, and libtool bdeps.
  * Update pot file during build; bdep on intltool.

  [ LI Daobing ]
  * debian/control: bump standards version to 3.8.3.

 -- LI Daobing <lidaobing@debian.org>  Tue, 25 Aug 2009 19:19:26 +0800

ibus-anthy (1.2.0.20090813-1) unstable; urgency=low

  * new upstream release.
  * debian/control: change vcs url.

 -- LI Daobing <lidaobing@debian.org>  Thu, 13 Aug 2009 21:48:20 +0800

ibus-anthy (1.2.0.20090804-1) unstable; urgency=low

  * new upstream release.

 -- LI Daobing <lidaobing@debian.org>  Wed, 05 Aug 2009 20:31:23 +0800

ibus-anthy (1.2.0.20090617-1) unstable; urgency=low

  * new upstream release.
  * debian/control:
    - depends on ibus >= 1.2
    - bump standards version to 3.8.2

 -- LI Daobing <lidaobing@debian.org>  Sat, 20 Jun 2009 23:15:11 +0800

ibus-anthy (1.1.0.20090603-1) unstable; urgency=low

  * new upstream release.

 -- LI Daobing <lidaobing@debian.org>  Sat, 06 Jun 2009 16:58:31 +0800

ibus-anthy (1.1.0.20090402-1) unstable; urgency=low

  * initial release to Debian (closes: #521627)
  * new upstream release.
  * debian/debian/patches/01_rpath.dpatch: merged by upstream, removed.
  * no patch is needed. remove dpatch from debian/*
  * debian/control:
    - bump standards version to 3.8.1.
    - change maintainer's email.
    - add ${python:Depends} to depends.

 -- LI Daobing <lidaobing@debian.org>  Thu, 23 Apr 2009 19:47:11 +0800

ibus-anthy (0.1.1.20080912-0ubuntu1) jaunty; urgency=low

  * Initial release (LP: #312715)
  * debian/patches/01_rpath.dpatch: remove rpath in Makefile.am

 -- LI Daobing <lidaobing@gmail.com>  Fri, 13 Feb 2009 20:41:26 +0800
